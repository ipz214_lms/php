<?php


class Student extends Human
{
    private $univ;
    private $course;
    function __construct($name, $univ, $course = 1, $age = 1, $phone = "")
    {
        $this->univ = $univ;
        parent::__construct($name, $age, $phone);
    }
    function setUniv($univ) {
        $this->univ = $univ;
    }
    function getUniv() {
        return $this->univ;
    }
    function courseUp() {
        $this->course++;
    }
    function __toString(): string
    {
        return parent::__toString() . "[ univ: {$this->univ}, course: {$this->course}]";
    }
    public function makeChild()
    {
        return "{$this->name} makeChild";
    }
    /**
     * @return mixed
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param mixed $course
     */
    public function setCourse($course): void
    {
        $this->course = $course;
    }
}