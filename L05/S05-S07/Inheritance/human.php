<?php

interface IClean {
    function cleanHome();
    function cleanKitchen();
}


abstract class Human implements IClean
{
    public $name;
    protected $age;
    private $phone;
    private $id;
    private static $next;
    function __construct($name, $age = 1, $phone = "") {
        $this->name = $name;
        $this->age = $age;
        $this->phone = $phone;
        $this->setId();
    }
    public function cleanHome()
    {
        return "{$this->name} is cleaning Home";
    }
    public function cleanKitchen()
    {
        return "{$this->name} is cleaning Kitchen";
    }
    protected abstract function makeChild();

    public function talk(){
        echo "{$this->name} is talking";
    }

    protected function walk(){
        echo "{$this->name} is walking";
    }

    private function swim(){
        echo "{$this->name} is swimming";
    }
    function __toString(): string
    {
        return "Object Human {$this->id}: [name: {$this->name},age: {$this->age}, phone: {$this->phone}]";
    }

    /**
     * @return int|mixed
     */
    public function getAge(): mixed
    {
        return $this->age;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed|string
     */
    public function getPhone(): mixed
    {
        return $this->phone;
    }

    /**
     * @param int|mixed $age
     */
    public function setAge(mixed $age): void
    {
        $this->age = $age;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @param mixed|string $phone
     */
    public function setPhone(mixed $phone): void
    {
        $this->phone = $phone;
    }
    private function setId(): void
    {
        $this->id = self::$next;
        self::$next++;
    }
}