<?php

include "Human.php";
include "Student.php";
include "Programmer.php";


echo "<pre>";
    $student = new Student('Ivan', 'ZTU',);
    $prog = new Programmer("Dan", age: 400, phone: "2131231");
    $prog->setLanguages(['C', 'C++']);
    echo $student . "\n";
    echo $prog . "\n";
    echo $student->makeChild() . "\n";
    echo $prog->makeChild() . "\n";
    echo $student->cleanHome() . "\n";
    echo $prog->cleanKitchen() . "\n";
echo "</pre>";