<?php


class Programmer extends Human
{
    private array $languages = [];

    public function __construct($name, $age = 1, $phone = "")
    {
        parent::__construct($name, $age, $phone);
    }
    function __toString(): string
    {
        return parent::__toString() . "[languages: " . implode(',', $this->languages) . "]";
    }
    public function makeChild()
    {
        return "{$this->name} makeChild";
    }

    /**
     * @return array
     */
    public function getLanguages(): array
    {
        return $this->languages;
    }

    /**
     * @param array $languages
     */
    public function setLanguages($languages): void
    {
        array_push($this->languages, ...$languages);
    }

}