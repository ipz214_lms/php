<?php

class Rectangle
{
    private $x, $y;
    private $width;
    public $id;
    public static $next = 0;

    function __construct($width, $x = 0, $y = 0)
    {
        $this->setId();
        $this->width = $width;
        $this->x = $x;
        $this->y = $y;
    }
    function __clone() {
        $this->setId();
    }
    function __toString(): string
    {
       return "Object Rectangle (ID: {$this->id}, : [width: {$this->width}, [x: {$this->x}, y: {$this->y}]" . self::$next;
    }

    /**
     * @return int
     */
    /**
     * @param int $id
     */
    private function setId(): void
    {
        $this->id = self::$next;
        self::$next++;
    }
}
if (basename(__FILE__) == basename($_SERVER["SCRIPT_FILENAME"])) {
    $o1 = new Rectangle(10);
    $o2 = new Rectangle(5);
    $o3 = clone $o1;
    $o4 = clone $o2;
    echo "<pre>";
    echo $o1 . "\n";
    echo $o2 . "\n";
    echo $o3 . "\n";
    echo $o4 . "\n";
    echo "</pre>";
}