<?php

class Circle
{
    private float $x, $y;
    private float $r;

    function __construct($r, $x = 0, $y = 0)
    {
        $this->x = $x;
        $this->y = $y;
        $this->r = $r;
    }

    function __toString(): string
    {
        return "Коло з центром в ({$this->x}, {$this->y}) і радіусом {$this->r}";
    }

    /**
     * @return float
     */
    public function getR(): float
    {
        return $this->r;
    }

    /**
     * @return int|mixed
     */
    public function getY(): mixed
    {
        return $this->y;
    }

    /**
     * @param int|mixed $x
     */
    public function setX(mixed $x): void
    {
        $this->x = $x;
    }

    /**
     * @param int|mixed $y
     */
    public function setY(mixed $y): void
    {
        $this->y = $y;
    }

    public function cross(Circle $circle): bool
    {
        $d = sqrt(pow(($this->x - $circle->x), 2)
            + pow(($this->y - $circle->y), 2));
        return $d < pow($this->r + $circle->r, 2) && $d > pow($this->r - $circle->r, 2);
    }

}

if (basename(__FILE__) == basename($_SERVER["SCRIPT_FILENAME"])) {
    $c1 = new Circle(10);
    $c2 = new Circle(9, 2, 3);

}
?>
<pre>
    <?php
    echo $c1 . "\n";
    echo $c2 . "\n";
    echo "Is intersect:" . $c1->cross($c2);
    ?>
</pre>
