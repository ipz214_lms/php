<?php

class Dir_text
{
    static string $dir="text";
    static function read($file): string|bool
    {
        return file_get_contents(self::path($file));
    }
    static function write($file, $content): void
    {
        file_put_contents(self::path($file), $content);
    }
    static function clear($file): void
    {
        file_put_contents(self::path($file), "");
    }
    private static  function path($file): string
    {
        return implode("/", [self::$dir , $file]);
    }
}
if (basename(__FILE__) == basename($_SERVER["SCRIPT_FILENAME"])) {
    $f = new Dir_text();
    echo "<pre>";
    echo "Read:" . $f::read("2.txt") . "\n";
    echo "Write:" . $f::write("1.txt", "write 1.txt") . "\n";
    echo "Read after Write:" . $f::read("1.txt") . "\n";
    echo "Clear:" . $f::clear("1.txt") . "\n";
    echo "Read after Clear:" . $f::read("1.txt") . "\n";
    echo "</pre>";
}