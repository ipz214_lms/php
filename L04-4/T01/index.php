<?php
// MAIN LOGIC
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $exp = "/(ht|f)tp(s?):\\/\\/[0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*(:(0-9)*)*(\\/?)([a-zA-Z0-9\\-=.?,'\\/\\\\+&amp;%$#_]*)?
/ui";

    $replaced_str = preg_replace($exp, "тут була адреса сайту\n", $_POST['urls']);
    preg_match_all($exp, $_POST['urls'], $urls);
}
// FORM SECTION
if ($_SERVER['REQUEST_METHOD'] == 'GET'): ?>
    <form action="" method="post">
        <p><textarea name="urls" id="urls" cols="80" rows="10">
        Створіть текстове поле, опрацюйте форму і замініть всі знайдені адреси
сайтів в тексті на «тут була адреса сайту»
Всі знайдені адреси зберігайте в окремий масив. Виведіть отриманий текст і
масив з адресами
Приклади сайтів:
http://www.w3schools.com/php/php_ref_regex.asp
https://learn.ztu.edu.ua/course/view.php?id=4750
http://translate.google.com
https://www.php.net/manual/ru/function.scandir.php
http://www.microsoft.com/uk-ua
    </textarea></p>
        <p>
            <button type="submit">Submit</button>
        </p>

    </form>
<?php endif;
// RESULT OUTPUT
if($_SERVER['REQUEST_METHOD'] == 'POST'): ?>
<pre>
    <?= $replaced_str ?>
</pre>
<pre>
    <?php print_r($urls[0]); ?>
</pre>
<?php endif;?>