<?php
// MAIN LOGIC
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    session_start();
    // DATA TO SHOW
    foreach ($_POST as $key => $value) {
        $_SESSION[$key] = $value;
    }
    $keys = array('name', 'passwords1', 'passwords2', 'city', 'sex', 'games', 'about', 'photo');
    $data = array_fill_keys($keys, '');

    foreach ($_SESSION as $key => $value) {
        $data[$key] = $value;
    }

    // FILE HANDLING
    if ($_FILES && $_FILES["photo"]["error"] == UPLOAD_ERR_OK) {
        $name = "upload/" . $_FILES["photo"]["name"];
        move_uploaded_file($_FILES["photo"]["tmp_name"], $name);
        $data['photo'] = "<img src = '{$name}'>";
    }
    // htmlentities
    function html_escape(&$item)
    {
        $item = htmlentities($item, ENT_QUOTES);
    }

    array_walk_recursive($_POST, "html_escape");


}
// SHOW DATA
if ($_SERVER['REQUEST_METHOD'] === 'POST'): ?>
    <table>
        <?php foreach ($data as $key => $value): ?>
            <tr>
                <th><?= $key ?></th>
                <td><?= is_array($value) ? implode(',', $value) : $value ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>
