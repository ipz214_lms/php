<?php session_start();
if (isset($_GET['lang'])) {
    setcookie('lang', $_GET['lang'],expires_or_options: time() + 15638400, path: "/");
    header('Location: index.php');
}
?>
<?php
require 'makehtml.php';
$keys = array('name', 'passwords1', 'passwords2', 'city', 'sex', 'games', 'about', 'photo');
$sticky_v = array_fill_keys($keys, '');

$male = $female = '';
foreach ($_SESSION as $key => $value) {
    $sticky_v[$key] = $value;
    if ($key == 'sex') {
        $male = $_SESSION['sex'] == 'Чоловік' ? 'checked' : '';
        $female = $_SESSION['sex'] == 'Жінка' ? 'checked' : '';
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Завдання 4</title>
    <link rel="stylesheet" href="../css/T03.css">
    <link rel="stylesheet" href="../css/T02.php">
    <link rel="stylesheet" href="../css/T04.css">
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/gh/lipis/flag-icons@6.6.6/css/flag-icons.min.css"
    />
</head>
<body>
<span class="lang">
    <a href="index.php?lang=jp"><span class="fi fi-jp"></span></a>
    <a href="index.php?lang=gb"><span class="fi fi-gb"></span></a>
    <a href="index.php?lang=us"><span class="fi fi-us"></span></a>
    <a href="index.php?lang=fr"><span class="fi fi-fr"></span></a>
    <span><?= $_COOKIE['lang'] ?? null ? 'Вибрана мова: ' . $_COOKIE['lang'] : '' ?></span>
</span>
<div class="container">
    <form action="show_form.php" enctype="multipart/form-data" method="post">
        <div class="row">
            <div class="col-25">
                <label for="name">Логін</label>
            </div>
            <div class="col-75">
                <input type="text" id="name" name="name" value="<?= $sticky_v['name'] ?>">
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="password">Пароль</label>
            </div>
            <div class="col-75">
                <input type="password" id="password" name="passwords1" value="<?= $sticky_v['passwords1'] ?>">
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="repassword">Пароль (ще раз)</label>
            </div>
            <div class="col-75">
                <input type="password" id="repassword" name="passwords2" value="<?= $sticky_v['passwords2'] ?>">
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="city">Місто</label>
            </div>
            <div class="col-75">
                <?= createSelect('city'
                    , array('житомир' => 'Житомир', 'київ' => 'Київ')
                    , $sticky_v['city']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="male">Стать</label>
            </div>
            <div class="col-75">
                <input type="radio" id="male" name="sex" value="Чоловік" <?= $male ?>>
                <label for="male">Чоловік</label>
                <input type="radio" id="female" name="sex" value="Жінка" <?= $female ?>>
                <label for="female">Жінка</label>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="football">Улюблені ігри</label>
            </div>
            <div class="col-75">
                <p><input type="checkbox" name="games[]" id="football"
                          value="Футбол" <?= $sticky_v['games'][0] ?? null ?  'checked' : ''?>>
                    <label for="football">Футбол</label></p>
                <p><input type="checkbox" name="games[]" id="basketball"
                          value="Баскетбол" <?= $sticky_v['games'][1] ?? null ?  'checked' : ''?>>
                    <label for="basketball">Баскетбол</label></p>
                <p><input type="checkbox" name="games[]" id="volleyball"
                          value="Волейбол" <?= $sticky_v['games'][2] ?? null ?  'checked' : ''?>>
                    <label for="volleyball">Волейбол</label></p>
                <p><input type="checkbox" name="games[]" id="chess"
                          value="Шахи" <?= $sticky_v['games'][3] ?? null ?  'checked' : ''?>>
                    <label for="chess">Шахи</label></p>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="about">Про себе</label>
            </div>
            <div class="col-75">
                <textarea name="about" id="about"><?= ($sticky_v['about']) ?></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="photo">Фотографія</label>
            </div>
            <div class="col-75">
                <input type="file" name="photo" id="photo">
            </div>
        </div>
        <div class="row">
            <input type="submit" value="Зареєструватися">
        </div>
    </form>

</div>
</body>
</html>