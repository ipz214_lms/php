<?php
function makeCheckboxes($name, $query, $options)
{
    $checkboxes = array();
    foreach ($options as $value => $label) {
        $checked = in_array($value, $query) ? "checked" : "";

        array_push($checkboxes,"<input type=\"checkbox\" name=\"{$name}\"
        value=\"{$value}\" {$checked}" );
    }
}

function createSelect($name, $values, $selected = null)
{
    $resultString = "<select name=\"{$name}\">";
    foreach ($values as $value => $title) {
        if ($selected == $title or $selected == $value)
            $resultString .= "<option selected = \"selected\" value={$value}>{$title}</option>";
        else
            $resultString .= "<option value={$value}>{$title}</option>";
    }
    return $resultString .= '</select>';
}