<?php
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Завдання 3</title>
    <link rel='stylesheet' type='text/css' href='../css/T02.php'/>
</head>
<body>
<form action="login.php" method="post">

    <?php
    if (isset($_SESSION["errorMessage"])) {
        echo $_SESSION["errorMessage"];
        unset($_SESSION["errorMessage"]);
    }
    ?>

    <p><label>Логін:<input type="text" name="login" required placeholder="Логін"></label></p>
    <p><label>Пароль:<input type="password" name="password" required placeholder="Пароль"></label></p>
    <p><input type="submit" value="submit"></p>
</form>
</body>
</html>
