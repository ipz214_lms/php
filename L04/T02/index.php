<?php
if (isset($_GET['fontsize'])) {
    setcookie('fontsize', $_GET['fontsize'], path: "/");
    header('Location: index.php');
    // Дивитися ../css/T02.php
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Завдання 2</title>
    <link rel='stylesheet' type='text/css' href='../css/T02.php' />
</head>
<body>
<a href="index.php?fontsize=large">Великий шрифт</a>
<a href="index.php?fontsize=medium">Середній шрифт</a>
<a href="index.php?fontsize=small">Маленький шрифт</a>
</body>
</html>
