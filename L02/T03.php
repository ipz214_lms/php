<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<p>1) Написати функцію, яка приймає масив. Вивести елементи масиву, що повторюються.</p>
<?php print_r(get_duplicates([1,2,3,3,3,1])); ?>
<p>3) Створіть 2 масиви за допомогою функції createArray() та продемонструйте виконання функції з 2-го пункту
</p>
<pre>
  <?php 
    $array1 = create_array();
    $array2 = create_array();
    echo print_r($array1, true) . print_r($array2, true);
    calculate_arrays($array1,$array2);
  ?>
</pre>
</body>
</html>
<?php

function get_duplicates($array) {
  return array_diff_assoc($array, array_unique($array));
}

function create_array() {
  $size = mt_rand(3,7);
  for($i = 0; $i < $size; $i++) {
    $array[] = mt_rand(10,20);
  }
  return $array;
}

function calculate_arrays($array1, $array2) {
  $combined_array = array_merge($array1 , $array2);
  echo "Зєднує обидва масиви в один:" . print_r($combined_array, true) . "\n";

  $unique_array = array_unique($combined_array);
  echo "Вилучає повторювані елементи:" . print_r($unique_array, true);
  
  $sorted_array = sort($combined_array);
  echo "Відсортовує отриманий масив за зростанням:" . print_r($combined_array, true) . "\n";
  return $sorted_array;

}