<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        table {
            width: 100%;
            text-align: center;
        }

        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            border-spacing: 0;
            padding: 0;
        }

        th {
            background-color: rgb(238, 238, 82);
        }
    </style>
</head>
<body>
<table>
    <tr>
        <th>sin(x)</th>
        <th>cos(x)</th>
        <th>tg(x)</th>
        <th>my_tg(x)</th>
        <th>x<sup>y</sup></th>
        <th>x!</th>
        <th>average(x,y)</th>
    </tr>
    <tr>
        <td>
            <?php if (isset($_POST["x"])) echo sin($_POST["x"]); ?>
        </td>
        <td>
            <?php if (isset($_POST["x"])) echo cos($_POST["x"]) ?>
        </td>
        <td>
            <?php if (isset($_POST["x"])) echo tan($_POST["x"]) ?>
        </td>
        <td>
            <?php if (isset($_POST["x"])) echo my_tg($_POST["x"]) ?>
        </td>
        <td>
            <?php if (isset($_POST["x"])) echo my_pow($_POST["x"], $_POST["y"]) ?>
        </td>
        <td>
            <?php if (isset($_POST["x"])) echo my_factorial($_POST["x"]) ?>
        </td>
        <td>
            <?php if (isset($_POST["x"]) && isset($_POST["y"])) echo my_average($_POST["x"], $_POST["y"]) ?>
        </td>
    </tr>
</table>
<form action="" method="post">

    <label for="x">X

        <input type="text" name="x" id="x" required>
    </label>
    <label for="y">Y

        <input type="text" name="y" id="y" required>
    </label>
    <button type="submit">=</button>
</form>
</body>
</html>
<?php

function my_tg($x)
{
    return tan($x);
}

function my_pow($x, $y)
{
    return $x ** $y;
}

function my_factorial($number)
{
    if ($number <= 1) {
        return 1;
    } else {
        return $number * my_factorial($number - 1);
    }

}

function my_average($x, $y)
{
    return ($x + $y) / 2;
}