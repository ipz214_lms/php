<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        table {
            table-layout: fixed;
            width: 100%;
            text-align: center;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            border-spacing: 0;
            padding: 0;
        }
        td {
            width: 25%;
            height: 50px;
        }
        th {
            background-color: rgb(238, 238, 82);
        }
        .black_canvas {
            display: block;
            position: relative;
            background: black;
            overflow: hidden;
            aspect-ratio: 1 / 1;
        }
        .square {
            display: block;
            background-color: darkred;
            aspect-ratio: 1 / 1;
            position: absolute;
        }
    </style>
</head>
<body>
<?php print_table(3,5); ?>
<?php print_squares(5,"500px"); ?>
</body>
</html>
<?php

function print_table($r, $c) {
    echo "<table>";
    for($i = 0; $i < $r; $i++) {
        echo "<tr>";
        for($j = 0; $j < $c; $j++) {
            $color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
            echo "<td style='background-color:$color'></td>";
        }
        echo "</tr>";
    }
    echo '</table>';
}

function print_squares($n, $canvas_size) {
    echo "<div class='black_canvas' style='width: {$canvas_size}'>";
        for($i = 0; $i < $n; $i++) {
            $size = mt_rand(15, $canvas_size / 4) . "px"; # Розмір квадрата
            $position = array(mt_rand(0, $canvas_size - $size) . "px", mt_rand(1, $canvas_size - $size) . "px"); # (x,y)

            echo "<div class='square' style='width: $size; top: $position[0]; left: $position[1]'></div>";
        }
    echo "</div>";
}

?>